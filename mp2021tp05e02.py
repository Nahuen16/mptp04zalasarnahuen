'''
a. Agregar vehículos, los atributos del mismo son:
    Dominio: String entre 6 y 9 caracteres sin espacios
    Marca: (R=Renault, F=Ford, C=Citroen)
    Tipo: U=Utilitario, A=Automóvil
    Modelo: en el rango [2005, 2020]
    Kilometraje
    Precio valuado: representa el precio de valuación del vehículo según 
    su año de fabricación y estado.
    Precio de venta: Es de solo lectura y representa el precio de venta 
    del vehículo y su valor es un 10% más que el precio valuado.
    Estado: (V=Vendido, D=Disponible, R=Reservado). Este valor no se debe 
    ingresar, por defecto es disponible (D).
b. Reservar un automóvil: implica cambiar el estado del vehículo a Reservado (R)
c. Buscar un automóvil por su dominio
d. Ordenar la lista de automóviles en forma ascendente o descendente por Marca
e. Ordenar la lista de automóviles en forma ascendente o descendente por Precio de venta
   mostrando solamente los que se encuentren disponibles.
Consideraciones
- El programa debe cargar la lista en con al menos 10 automóviles en el momento en que se inicia.
- Realice todas las validaciones necesarias para el punto a.
'''

import os
import random

def limpiarPantalla():
    if os.name == 'nt': # windows
        borrar = 'cls'
    else:
        borrar = 'clear'  # linux, unix, macOs
    os.system(borrar)

def continuar():
    print()
    input('Presione una tecla para continuar ')
    limpiarPantalla()

def datosIniciales():
    lista = []
    n = int(input('Ingrese cantidad de datos iniciales: '))
    for i in range(n):
        dominio = str(random.randint(1000000,10000000))
        marcaCad = 'RFC'
        marca = random.choice(marcaCad)
        tipoCad = 'UA'
        tipo = random.choice(tipoCad)
        modelo = random.randint(2005,2021)
        precioValuado = round(float(random.randint(300000, 3000000)), 2)
        precioVenta = round(precioValuado * 1.1, 2)
        estadoCad = 'VDR'
        estado = random.choice(estadoCad)
        elem = [dominio, marca, tipo, modelo, precioValuado, precioVenta, estado]
        lista.append(elem)
    return lista

def menu():
    print('--- Menú de opciones ---')
    print()
    print('1. Agregar vehículos')
    print('2. Mostrar vehículos')
    print('3. Reservar un automóvil')
    print('4. Buscar un automóvil por su dominio')
    print('5. Ordenar por Marca A/D')
    print('6. Ordenar disponibles por Precio de venta A/D')
    print('9. Salir')
    opc = ''
    while opc not in ['1', '2', '3', '4', '5', '6', '9']:
        opc = input('-> ')
    limpiarPantalla()
    return opc

def mostrar(lista):
    for pos, elem in enumerate(lista):
        print(pos, elem)

#principal
limpiarPantalla()
opcion = '0'
#Llamado a datos iniciales
lista = datosIniciales()

while opcion != '9':
    opcion = menu()
    if opcion == '1':
        print('Agregar vehículos')
  
    elif opcion == '2':
        print('Mostrar vehículos')
        mostrar(lista)
    elif opcion == '3':
        print('Reservar un automóvil')
      
    elif opcion == '4':
        print('Buscar un automóvil por su dominio') 

    elif opcion == '5':
        print('Ordenar por Marca A/D')

    elif opcion == '6':
        print('Ordenar disponibles por Precio de venta A/D')

    elif opcion == '9':        
        print('Fin del programa')
        
    else:
        print('Error')
    continuar()


